public class Fraction {
	private int a;
	private int b;
	private double c;
	
	public Fraction(int a,int b)
	{
		this.a=a;
		this.b=b;
		this.c=a/b;
	}
	public Fraction(int a)
	{
		this.a=a;
		this.c=a/1;
	}
	public Fraction()
	{
		this.c=0/1;
	}

	public int getA() {
		return a;
	}

	public int getB() {
		return b;
	}

	public double getC() {
		return c;
	}

	@Override
	public String toString() {
		return "Fraction [a=" + a + ", b=" + b + ", c=" + c + "]";
	}
	
}
